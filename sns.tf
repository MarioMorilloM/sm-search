# Topic, Subscription and Quere related to a lambda function
resource "aws_sqs_queue" "isbn_queue" {
  # name                      = "isbn-queue"
  name                      = join("-",[var.project_name, var.environment, "queue"])
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10

  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

resource "aws_sns_topic" "textrack_isbn_topic" {
  # name = "AmazonTextractTopicSM1"
  name = join("-",[var.project_name, var.environment, "Textract-Topic"])
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

resource "aws_sns_topic_subscription" "textrack_isbn_subscription" {
  topic_arn = aws_sns_topic.textrack_isbn_topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.isbn_queue.arn
}
