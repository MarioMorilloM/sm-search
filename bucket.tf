locals {
  # Name of the bucket that will contain the front
  front-name = join("-",["horus", var.project_name, var.environment, "bucket"])
}
# Bucket that will contain the front.
# The bucket parameter (name) is used in the policy
resource "aws_s3_bucket" "bucket-front" {
  bucket = local.front-name
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PublicReadGetObject",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${local.front-name}/*"
    }
  ]
}
EOF
  website {
   index_document = "index.html"
   error_document = "index.html"
  }

  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

# Bucket for the application that will contain the app for Beanstalk
resource "aws_s3_bucket" "bucket-app" {
  # bucket = var.isbn_beanstalk_app
  bucket = join("-",[var.isbn_beanstalk_app, var.project_name, var.environment, "bucket"])

  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

# Object: application that will be ran in Beanstalk
resource "aws_s3_bucket_object" "object-app" {
  bucket = aws_s3_bucket.bucket-app.id
  key    = "app.zip"
  source = "app.zip"
}

# Bucket that will contain files and lambdas' code
resource "aws_s3_bucket" "files-lambdas" {
  # bucket = var.isbn_files_lambdas
  bucket = join("-",[var.isbn_files_lambdas, var.project_name, var.environment, "bucket"])
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

# Object: lambda code for ISBN_Extractor
resource "aws_s3_bucket_object" "extractor-app" {
  bucket = aws_s3_bucket.files-lambdas.id
  key    = "Lambdas/ISBN_Extractor.zip"
  source = "ISBN_Extractor.zip"
}

# Object: Lambda code for OCR.
resource "aws_s3_bucket_object" "ocr-app" {
  bucket = aws_s3_bucket.files-lambdas.id
  key    = "Lambdas/Lambda_Function.zip"
  source = "Lambda_Function.zip"
}
