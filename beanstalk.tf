resource "aws_elastic_beanstalk_application" "seach-isbn" {
  # name        = "seach-isbn-app"
  name        = join("-",[var.project_name, var.environment, "ebs-app"])
  description = "seach Isbn"
}

resource "aws_elastic_beanstalk_application_version" "default" {
  # name        = "seach-isbn-version-label"
  name        = join("-",[var.project_name, var.environment, "app-label"])
  application = aws_elastic_beanstalk_application.seach-isbn.name
  description = "Isbn search app version created by Terraform"
  bucket      = aws_s3_bucket.bucket-app.id
  key         = aws_s3_bucket_object.object-app.id
}

#Environment
resource "aws_elastic_beanstalk_environment" "env-test" {
  name                = var.environment
  application         = aws_elastic_beanstalk_application.seach-isbn.name
  solution_stack_name = "64bit Amazon Linux 2018.03 v4.13.1 running Node.js"
  version_label       = aws_elastic_beanstalk_application_version.default.name

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "beanstalk-ec2-role"
  }
  # General Options: https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
  # Ec2Instances
  setting {
    namespace = "aws:ec2:instances"
    name = "InstanceTypes"
    value = var.instance_beanstalk_type
  }
  # Options specific - https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-specific.html
  # Proxy, Command, Compresion ...
  setting {
    namespace = "aws:elasticbeanstalk:container:nodejs"
    name = "ProxyServer"
    value = "none"
  }
  setting {
    namespace = "aws:elasticbeanstalk:container:nodejs"
    name = "NodeCommand"
    value = "npm run start:prod"
  }
  setting {
    namespace = "aws:elasticbeanstalk:container:nodejs"
    name = "GzipCompression"
    value = true
  }
  ## Autoscaling? Default values --> minSize 1, maxSize 4
  ## ELB Scheme --> Default public
  ## Default values reference: https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
}
