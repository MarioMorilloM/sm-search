# iam roles
# Beanstalk IAM profile role launch_configuration
resource "aws_iam_role" "beanstalk-ec2-role" {
    name = "beanstalk-ec2-role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}
resource "aws_iam_instance_profile" "beanstalk-ec2-role" {
    name = "beanstalk-ec2-role"
    role = aws_iam_role.beanstalk-ec2-role.name
}

# attach policies
resource "aws_iam_policy_attachment" "app-attach1" {
    name = "app-attach1"
    roles = [aws_iam_role.beanstalk-ec2-role.name]
    policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}
resource "aws_iam_policy_attachment" "app-attach2" {
    name = "app-attach2"
    roles = [aws_iam_role.beanstalk-ec2-role.name]
    policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker"
}
resource "aws_iam_policy_attachment" "app-attach3" {
    name = "app-attach3"
    roles = [aws_iam_role.beanstalk-ec2-role.name]
    policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
}

# Roles for lambdas
resource "aws_iam_role" "iam_for_isbn_getText" {
  name = "iam_for_isbn_getText"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

resource "aws_iam_role" "iam_for_isbn_ocr" {
  name = "iam_for_isbn_ocr"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

resource "aws_iam_role" "iam_for_isbn_sns_ocr" {
  name = "SNS_OCR"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}


# IAM
resource "aws_iam_role" "api-gw-lambda-integration" {
  name = "myrole"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}
