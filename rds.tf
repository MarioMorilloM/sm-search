data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.default.id
  name   = "default"
}

resource "aws_db_instance" "default" {
  engine             = "mysql"
  engine_version     = "5.7.19"
  allocated_storage  = 20
  storage_type       = "gp2"
  storage_encrypted  = false

  instance_class = var.instance_rds_class

  name           = var.rds_name
  username       = var.database_user
  password       = var.database_password
  port           = "3306"

  vpc_security_group_ids   = [data.aws_security_group.default.id]
  availability_zone        = var.availability_zone

  skip_final_snapshot = true

  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}
