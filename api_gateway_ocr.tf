resource "aws_api_gateway_rest_api" "Ocr-API" {
  # name        = "OCR-API"
  name        = join("-",["ocr", var.project_name, var.environment, "api"])
  description = "Api Gateway for Ocr to Lambda"

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  tags = {
    Project = var.project_name
    Environment = var.environment
  }

}

resource "aws_api_gateway_resource" "Ocr-API-Resource" {
  rest_api_id = aws_api_gateway_rest_api.Ocr-API.id
  parent_id   = aws_api_gateway_rest_api.Ocr-API.root_resource_id
  path_part   = "OCR_ISBN"
}

resource "aws_api_gateway_method" "Ocr-API-Method" {
  rest_api_id   = aws_api_gateway_rest_api.Ocr-API.id
  resource_id   = aws_api_gateway_resource.Ocr-API-Resource.id
  http_method   = "ANY"
  authorization = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_method_response" "ocr_response_200" {
  rest_api_id = aws_api_gateway_rest_api.Ocr-API.id
  resource_id = aws_api_gateway_resource.Ocr-API-Resource.id
  http_method = aws_api_gateway_method.Ocr-API-Method.http_method
  status_code = "200"

  response_models = {
         "application/json" = "Empty"
    }

}

# https://www.terraform.io/docs/providers/aws/r/api_gateway_api_key.html
resource "aws_api_gateway_api_key" "Ocr-api-key" {
  # name = "OCR_ISBN-Key"
  name        = join("-",["ocr", var.project_name, var.environment, "key"])
  description = "Created for OCR Lambda"
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

resource "aws_api_gateway_deployment" "ocr-deployment" {
  depends_on  = [aws_api_gateway_integration.Ocr-lambda-integration]
  rest_api_id = aws_api_gateway_rest_api.Ocr-API.id
  stage_name  = join("-",["ocr", var.project_name, var.environment, "deploy"])
}

resource "aws_api_gateway_stage" "ocr-stage" {
  stage_name    = var.environment
  rest_api_id   = aws_api_gateway_rest_api.Ocr-API.id
  deployment_id = aws_api_gateway_deployment.ocr-deployment.id

  tags = {
    Project = var.project_name
    Environment = var.environment
  }

}

resource "aws_api_gateway_usage_plan" "ocr-usage" {
  # name         = "Ocr-Isbn-UsagePlan"
  name        = join("-",["ocr", var.project_name, var.environment, "usagePlan"])
  description  = "Created for OCR Lambda"

  api_stages {
    api_id = aws_api_gateway_rest_api.Ocr-API.id
    stage  = aws_api_gateway_deployment.ocr-deployment.stage_name
  }

  tags = {
    Project = var.project_name
    Environment = var.environment
  }
  
}

resource "aws_api_gateway_usage_plan_key" "ocr-usage-key" {
  key_id        = aws_api_gateway_api_key.Ocr-api-key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.ocr-usage.id
}

######
# Lambda integration
######
# https://docs.aws.amazon.com/apigateway/api-reference/resource/integration/
resource "aws_api_gateway_integration" "Ocr-lambda-integration" {
  rest_api_id             = aws_api_gateway_rest_api.Ocr-API.id
  resource_id             = aws_api_gateway_resource.Ocr-API-Resource.id
  http_method             = aws_api_gateway_method.Ocr-API-Method.http_method
  integration_http_method = "POST" # Post or any?
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.ocr_ISBNs.invoke_arn
}

resource "aws_api_gateway_integration_response" "ocr-integration-response" {
  rest_api_id = aws_api_gateway_rest_api.Ocr-API.id
  resource_id = aws_api_gateway_resource.Ocr-API-Resource.id
  http_method = aws_api_gateway_method.Ocr-API-Method.http_method
  status_code = aws_api_gateway_method_response.ocr_response_200.status_code
  depends_on  = [aws_api_gateway_integration.Ocr-lambda-integration]

  response_templates = {
       "application/json" = ""
   }

}

# Lambda Permission
resource "aws_lambda_permission" "apigw_lambda_ocr" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ocr_ISBNs.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region}:${var.account_id}:${aws_api_gateway_rest_api.Ocr-API.id}/*/${aws_api_gateway_method.Ocr-API-Method.http_method}${aws_api_gateway_resource.Ocr-API-Resource.path}"
}
