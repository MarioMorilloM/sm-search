output "rds_availability_zone" {
  description = "The availability zone of the RDS instance"
  value       = aws_db_instance.default.availability_zone
}

output "rds_address" {
  description = "The address of the RDS instance"
  value       = aws_db_instance.default.address
}

output "rds_endpoint" {
  description = "The connection endpoint"
  value       = aws_db_instance.default.endpoint
}

output "rds_name" {
  description = "The database name"
  value       = aws_db_instance.default.name
}

output "rds_username" {
  description = "The master username for the database"
  value       = aws_db_instance.default.username
}

output "rds_port" {
  description = "The database port"
  value       = aws_db_instance.default.port
}

output "elastic_beanstalk_env_name" {
  description = "The Elastic Beanstalk Environment name"
  value       = aws_elastic_beanstalk_environment.env-test.name
}

output "elastic_beanstalk_env_application" {
  description = "The Elastic Beanstalk Environment Application"
  value       = aws_elastic_beanstalk_environment.env-test.application
}

output "elastic_beanstalk_env_endpoint_url" {
  description = "The Elastic Beanstalk Environment End Point URL"
  value       = aws_elastic_beanstalk_environment.env-test.endpoint_url
}

output "ocr_lambda_invoke_arn" {
  description = "The Invoke ARN for the OCR Lambda Function"
  value       =  aws_lambda_function.ocr_ISBNs.invoke_arn
}

output "get_isbn_lambda_invoke_arn" {
  description = "The Invoke ARN for the Get ISBNs Lambda Function"
  value       =  aws_lambda_function.get_ISBNs.invoke_arn
}
