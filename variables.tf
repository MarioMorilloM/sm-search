# GENERAL
variable "account_id" { }
variable "access_key" { }
variable "secret_key" { }
variable "project_name" {
  type    = string
  default = "search-isbn"
}
variable "environment" {
  default = "test"
}
variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "availability_zone" {
  type    = string
  default = "eu-west-1a"
}

# BUCKETS
variable "isbn_beanstalk_app" {
  type = string
  default = "app"
}
variable "isbn_files_lambdas" {
  type = string
  default = "lambdas"
}

# RDS
variable "rds_name" {
  type = string
  default = "hades_sm_dev"
}
variable "database_password" { }
variable "database_user" {
  type = string
  default = "admin"
}

variable "instance_rds_class" {
  type = string
  default = "db.t2.small"
}

variable "instance_beanstalk_type" {
  type = string
  default = "t2.medium"
}
