resource "aws_lambda_function" "ocr_ISBNs" {
  # filename      = "Lambda_Function.zip"
  s3_bucket     = aws_s3_bucket.files-lambdas.id
  s3_key        = aws_s3_bucket_object.ocr-app.id
  function_name = join("-",["ocr", var.project_name, var.environment])
  role          = aws_iam_role.iam_for_isbn_ocr.arn
  handler       = "LambdaCode.lambda_handler"

  # source_code_hash = filebase64sha256("Lambda_Function.zip")
  #source_code_hash = filebase64sha256(aws_s3_bucket_object.ocr-app.id)

  runtime = "python3.7"

  environment {
    variables = {
      ACCESSKEYID = var.access_key
      SECRETACCESSKEY = var.secret_key
      BUCKET_NAME = var.isbn_files_lambdas
      directory = "InputFiles"
      role_arn = aws_iam_role.iam_for_isbn_sns_ocr.arn
    }
  }
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}

resource "aws_lambda_function" "get_ISBNs" {
  # filename      = "ISBN_Extractor.zip"
  s3_bucket     = aws_s3_bucket.files-lambdas.id
  s3_key        = aws_s3_bucket_object.extractor-app.id
  function_name = join("-",["getIsbn", var.project_name, var.environment])
  role          = aws_iam_role.iam_for_isbn_getText.arn
  handler       = "index.handler"

  #source_code_hash = filebase64sha256(aws_s3_bucket_object.extractor-app.id)

  runtime = "nodejs12.x"

  environment {
    variables = {
      ACCESSKEYID = var.access_key
      SECRETACCESSKEY = var.secret_key
      BUCKET_NAME =var.isbn_files_lambdas
      OCR_LAMDBA_URL = aws_lambda_function.ocr_ISBNs.invoke_arn
      X_API_KEY = "eSioiT1CKK293Fmj1Z0LwbWGKm82H9u1hQEHzGGb"
    }
  }
  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}
