# Terraform SM - isbn-search
Despliegue de la infraestructura necesaria para un proyecyo de búsqueda ISBN.
El despliegue se hace sobre AWS.

# IMPORTANTE:
Este deslpiegue incluye la subida de las aplicaciones y funciones lambda a distintos buckets.
Estas aplicaciones NO están incluidas en el repositorio.
Antes de ejecutar el script, descargar y copiar las aplicaciones.
```
ISBN_Extractor.zip  --> Lambda        --> https://tests-aws-techedge.s3.eu-west-3.amazonaws.com/ISBN_Extractor.zip
Lambda_Function.zip --> Lambda        --> https://tests-aws-techedge.s3.eu-west-3.amazonaws.com/Lambda_Function.zip
app.zip             --> Beanstalk app --> https://tests-aws-techedge.s3.eu-west-3.amazonaws.com/app.zip
```

# Configuración:
Creación de un archifo terraform.tfvars con las variables 'delicadas':
```
access_key = "YOUR ACCESS KEY"
secret_key = "YOUR SECRET KEY"
account_id = "YOUR ACCOUNT NUMBER"
database_password = "YOUR DB PASSWORD"
```

Adicionalmente conviene revisar el archivo variables.tf cuyo, en especial:
```
variable "project_name" {
  type    = string
  default = "search-isbn"
}
variable "environment" {
  default = "test"
}
variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "availability_zone" {
  type    = string
  default = "eu-west-1a"
}
```
## Ejecución
Para ejcutar este ejemplo
```
terraform plan
terraform apply
```
## Recursos que crea:
- Buckets para alojar funciones y aplicaciones.
- Roles y políticas (iam) asociados y necesarios en cada componente
- Api Gateways (trigger para lambdas)
- Beanstalk (environment, aplicación y versión)
- Amazon RDS
- Sns (comunicación entre lambdas)
## Archivos adicionales no-componentes:
- provider.tf (aws)
- variables.tf
